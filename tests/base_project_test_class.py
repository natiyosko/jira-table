import re

import pytest
from datetime import timedelta, date

from dateutil.relativedelta import relativedelta
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from appium_selenium_driver.elements_tools.select_element import SelectElement

from appium_selenium_driver.base_test_class import *
from import_pages import *
from tests.user import User
import datetime


class BaseProjectTestClass(BaseTestClass):
    Action_number = ''
    date = ''
    Performed_the_action = ''
    Action_for_number = ''
    supplier = ''
    product = ''
    price = ''

    the_first_balance = ""
    the_second_balance = ""
    balance_price = ""

    # pre conditions for tests to all classes
    def login(self):
        self.driver.tools.set_text(LoginPage.username, User.username)
        self.driver.tools.set_text(LoginPage.password, User.password)
        self.driver.tools.wait_and_click(LoginPage.submit_btn)

    def login_with_different_username_or_password(self, username, password):
        self.driver.tools.set_text(LoginPage.username, username)
        self.driver.tools.set_text(LoginPage.password, password)
        self.driver.tools.wait_and_click(LoginPage.submit_btn)

    def input_number_and_cancel(self):
        self.input_phone_number()
        self.verify_alert_phone_number(VerificationPage.alert_phone_number)
        self.driver.tools.wait_and_click(VerificationPage.alert_cancel_btn)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.pre_number, timeout=10)

    def input_number_and_verify(self, price):
        self.input_phone_number()
        self.verify_alert_phone_number(VerificationPage.alert_phone_number)
        self.driver.tools.wait_and_click(VerificationPage.alert_confirm_btn)
        self.verify_the_verification_number()
        self.add_user_number()
        self.click_on_charge_and_verify(price)

    def verify_the_verification_number(self):
        self.driver.tools.element_contain_text(User.phone[:3], VerificationPage.validation_pre_number)
        self.driver.tools.element_contain_text(User.phone[3:], VerificationPage.validation_number)

    def input_phone_number(self):
        self.driver.tools.set_text(VerificationPage.pre_number, User.phone[:3])
        self.driver.tools.set_text(VerificationPage.number, User.phone[3:])

    def click_on_charge_and_verify(self, price):
        self.check_balance_before_buying()
        self.driver.tools.wait_and_click(VerificationPage.virtual_charge_button)
        self.check_if_the_price_on_success_msg_matches(price)

    def manual_click_on_charge_and_verify(self):
        self.check_balance_before_buying()
        self.driver.tools.wait_and_click(VerificationPage.manual_charge_button)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.success_msg)

    def verify_alert_phone_number(self, alert_phone_number_selector):
        assert User.phone in self.driver.tools.get_text_from_element(alert_phone_number_selector)

    def pay_for_calling_card_vc(self, price, href, cancel_payment=False):
        self.wait_and_click_if_virtual()
        self.balance_price = float(price)
        self.get_details(href, price)
        self.click_on_calling_card_by_price(price)
        if cancel_payment:
            self.input_number_and_cancel()
        self.input_number_and_verify(price)
        self.click_on_print()
        self.waits_for_print_preview_and_goes_back()
        self.get_info()
        self.close_the_success_alert()
        self.check_balance_after_buying()

    def pay_for_calling_card_mc(self, price, href):
        self.wait_and_click_if_manual()
        self.balance_price = float(price)
        self.get_details(href, price)
        self.click_on_calling_card_by_price(price)
        self.manual_click_on_charge_and_verify()
        self.click_on_print()
        self.waits_for_print_preview_and_goes_back()
        self.get_info()
        self.close_the_success_alert()
        self.check_balance_after_buying()

    def wait_and_click_if_virtual(self):
        self.driver.tools.wait_and_click(selector=CompanySelectTypeCardsPage.virtual_card, timeout=10,
                                         raise_exception=False)

    def wait_and_click_if_manual(self):
        self.driver.tools.wait_and_click(selector=CompanySelectTypeCardsPage.manual_card, timeout=10,
                                         raise_exception=False)

    def click_on_calling_card_by_price(self, price_on_card):
        self.driver.tools.wait_and_click(
            selector=(
                CallingcardsPage.calling_cards[0], CallingcardsPage.calling_cards[1].format(str(price_on_card))))

    def check_if_the_price_on_success_msg_matches(self, price):
        alert_success_price = self.driver.tools.wait_for_element_and_get_attribute(
            (VerificationPage.price_on_the_success_msg[0],
             VerificationPage.price_on_the_success_msg[1].format(str(price))), attribute='textContent')
        assert str(price) in alert_success_price
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.success_msg)

    def click_favorite_tag(self):
        self.driver.tools.wait_and_click(FavoriteTag.favorite_tag)

    def click_favorite_tag_and_wait_btn_annul(self):
        self.click_favorite_tag()
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.alert_annul_btn)

    def click_favorite_tag_and_wait_btn_confirm(self):
        self.click_favorite_tag()
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.alert_confirm_btn)

    def click_favorite_tag_and_click_btn_confirm(self):
        self.click_favorite_tag()
        self.driver.tools.wait_and_click(VerificationPage.alert_confirm_btn)

    def click_btn_return(self):
        self.driver.tools.wait_and_click(ButtonReturn.btn_return)

    def function_then_find_the_misgeret(self):
        self.balance_misgeret = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_misgeret, attribute='innerText'))
        self.balance_misgeret_agurut = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_misgeret_agurut, attribute='innerText'))
        self.the_misgeret = self.balance_misgeret + self.balance_misgeret_agurut

    def function_then_find_the_itra(self):
        self.balance_itra = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_itra, attribute='innerText'))
        self.balance_itra_agurut = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_itra_agurut, attribute='innerText'))
        self.the_itra = self.balance_itra + self.balance_itra_agurut

    def function_then_find_the_itra_lenitzul(self):
        self.balance_itra_lenitzul = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_itra_lenitzul, attribute='innerText'))
        self.balance_itra_lenitzul_agurut = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_itra_lenitzul_agurut, attribute='innerText'))
        self.the_itra_lenitzul = self.balance_itra_lenitzul + self.balance_itra_lenitzul_agurut

    def function_then_find_the_nekudot_lenitzul(self):
        self.balance_nekudot_lenitzul = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_nekudot_lenitzul, attribute='innerText'))
        self.balance_nekudot_lenitzul_agurut = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(
                BalanceVerification.balance_nekudot_lenitzul_agurut, attribute='innerText'))
        self.the_nekudot_lenitzul = self.balance_nekudot_lenitzul + self.balance_nekudot_lenitzul_agurut

    def check_balance_before_buying(self):
        self.function_then_find_the_misgeret()
        self.function_then_find_the_itra()
        self.function_then_find_the_itra_lenitzul()
        self.function_then_find_the_nekudot_lenitzul()
        self.the_first_balance = self.the_itra

    def check_balance_after_buying(self):
        self.function_then_find_the_itra()
        the_second_balance = self.balance_itra + self.balance_itra_agurut
        result = float(self.the_first_balance) - float(the_second_balance)
        commission = float(self.balance_price) - float(result)
        commission_percent = float(commission) * 100 / self.balance_price
        vat = (self.balance_price / 1.17) * 0.17
        # TODO this_is_a_problem_because_we_dont_now_the_commission_(nati_yoskovitz)
        # assert result == self.balance_price, "problem peletok"

    def take_digits_from_string_and_get_down_the_commas(self, number_string):
        return float(number_string.replace(',', ''))

    def add_user_number(self):
        if self.driver.wait.wait_for_elements_to_be_present(VerificationPage.user_number, timeout=2,
                                                            raise_exception=False):
            self.driver.tools.set_text(VerificationPage.user_number, User.phone[3:])

    def click_favorite_tag_and_check_if_add_moadafim(self):
        favorites_cards_before = self.get_favorites_cards()
        if favorites_cards_before != 0:
            self.click_favorite_tag_and_click_btn_confirm()
            self.driver.wait.wait_for_element_to_be_present(Moadafim.product_name)
            favorites_after_delete_one_cards = self.get_favorites_cards()
            assert favorites_after_delete_one_cards == favorites_cards_before - 1
        else:
            self.click_favorite_tag_and_click_btn_confirm()
            self.driver.wait.wait_for_element_to_be_present(Moadafim.product_name)
            favorites_after_add_one_cards_to_favorite = self.get_favorites_cards()
            assert favorites_after_add_one_cards_to_favorite == favorites_cards_before + 1

    def get_favorites_cards(self):
        self.driver.wait.wait_for_element_to_be_present(Moadafim.product_name)
        favorites_before = self.driver.wait.wait_for_elements_to_be_present(Moadafim.favorites_cards,
                                                                            raise_exception=False)
        if favorites_before:
            return len(favorites_before)
        else:
            favorites_before = 0
            return favorites_before

    def click_on_print(self):
        self.driver.tools.wait_and_click(VerificationPage.print_btn)

    def waits_for_print_preview_and_goes_back(self):
        WebDriverWait(self.driver.driver, 40).until(ec.number_of_windows_to_be(2))
        self.driver.driver.back()

    def get_info(self):
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.print_btn)
        text = self.driver.tools.wait_for_element_and_get_attribute(PrintPage.info, attribute='textContent')
        split_txt = text.split(' ')
        self.Action_number = split_txt[2]
        self.date = split_txt[6] + " " + split_txt[5]
        self.Performed_the_action = split_txt[9] + " " + split_txt[10]
        self.Action_for_number = split_txt[13]
        self.supplier = self.reverse_string(self.driver.tools.wait_for_element_and_get_attribute(
            PrintPage.supplier, attribute='textContent'))
        product = self.driver.tools.wait_for_element_and_get_attribute(PrintPage.product, attribute='textContent')
        self.price = self.driver.tools.wait_for_element_and_get_attribute(PrintPage.price, attribute='textContent')
        self.product = self.reverse_string(product)

    def reverse_string(self, reverse_string):
        corrected_string = ''.join(s if s.isdigit() else s[::-1] for s in reversed(re.split('(\d+)', reverse_string)))
        return corrected_string

    def get_details(self, end_of_href, priceOnCard):
        price = str(priceOnCard)
        self.driver.tools.wait_and_click((CallingcardsPage.details_of_cards[0],
                                          CallingcardsPage.details_of_cards[1].format(num_of_picture=end_of_href,
                                                                                      price_on_detail=price)))
        self.check_and_close_details_alert(price)

    def check_and_close_details_alert(self, price_on_card):
        alert_price_details = self.driver.tools.wait_for_element_and_get_attribute(
            (CallingcardsPage.alert_details[0], CallingcardsPage.alert_details[1].format(price_on_card)),
            attribute='textContent')
        assert price_on_card in alert_price_details
        self.driver.tools.wait_and_click(VerificationPage.close_alert_detail_btn)

    def login_and_click_on_side_menu_option(self, option_from_menu):
        self.login()
        self.driver.tools.wait_and_click(
            (GeneralReportsPage.side_menu_options[0], GeneralReportsPage.side_menu_options[1].format(option_from_menu)))

    def close_the_success_alert(self):
        self.driver.tools.wait_and_click(VerificationPage.close_btn_in_charge_success_alert)

    def click_on_back_botton(self):
        self.driver.tools.wait_and_click(VerificationPage.back_botton)

    def after_the_click_back_button(self):
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.back_button_wait_before_the_close)

    def check_radio_buttons(self, selector):
        for item in reversed(range(4)):
            self.driver.tools.wait_and_click(
                (selector[0], selector[1].format(item)))

    def get_vat(self):
        self.driver.tools.wait_and_click(HomePage.configuration_Link)
        self.driver.tools.wait_and_js_click((GeneralConfigurationPage.config_link_options[0],
                                            GeneralConfigurationPage.config_link_options[1].format('general')))
        return int(self.driver.tools.wait_and_get_attribute_when_present(GeneralLink.vat_input, 'value'))

    def change_vat(self, new_vat):
        self.get_vat()
        self.driver.tools.clear_and_set_text(GeneralLink.vat_input, new_vat)
        self.driver.tools.wait_and_js_click(GeneralLink.save_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralLink.alert_successful)

    def insert_different_date_to_input_and_check(self, input_selector, months=-2, days=0):
        new_date = date.today() + relativedelta(months=months, days=days)
        # attention: this format "%m%d%Y" doesn't work on hebrew browser
        self.driver.tools.set_text(input_selector, new_date.strftime("%m%d%Y"))
        assert new_date.__str__() in self.driver.tools.wait_for_element_and_get_attribute(
            input_selector, "value")

    def date_expected_from_input(self, input_selector, months=0, days=0):
        expecting_date = date.today() + relativedelta(months=months, days=days)
        assert expecting_date.__str__() in self.driver.tools.wait_for_element_and_get_attribute(
            input_selector, "value")

    def click_on_input_and_choose_option(self, input_selector, choice_name):
        self.driver.tools.wait_and_click(input_selector)
        self.driver.tools.wait_and_click((MainSuppliersAndProductPage.choose_options_from_input_by_name[0],
                                          MainSuppliersAndProductPage.choose_options_from_input_by_name[1]
                                          .format(choice_name)))
        assert choice_name.replace(" ", "") in self.driver.tools.get_text_from_element(input_selector)\
            .replace(" ", ""), 'your choice is not in the input'


