from tests.test_reports.base_reports_test_class import *
import pytest
import re


class TestKvish6Report(BaseReportsTestClass):

    def login_and_get_in_kvish6_report(self):
        self.login_and_get_in_report("5", 'לכביש')

    def test_kvish6_report_login_and_get_in_kvish6_report(self):
        self.login_and_get_in_kvish6_report()
        self.remove_logs_dir = True

    def test_kvish6_report_input_start_date(self):
        self.login_and_get_in_kvish6_report()
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.remove_logs_dir = True

    def test_kvish6_report_input_end_date(self):
        self.login_and_get_in_kvish6_report()
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_kvish6_report_start_hour(self):
        self.login_and_get_in_kvish6_report()
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_kvish6_report_start_minute(self):
        self.login_and_get_in_kvish6_report()
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select, minute=2)
        self.remove_logs_dir = True

    def test_kvish6_report_end_hour(self):
        self.login_and_get_in_kvish6_report()
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_kvish6_report_end_minute(self):
        self.login_and_get_in_kvish6_report()
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select, minute=2)
        self.remove_logs_dir = True

    def test_kvish6_show_reports_btn(self):
        self.login_and_get_in_kvish6_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_kvish6_report_sales_summery_span(self):
        self.login_and_get_in_kvish6_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        assert bool(re.search(r'\d', self.driver.wait.wait_for_element_to_be_present(
            GeneralReportsPage.total_sales_span).text))
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - dates are currently not shown")
    def test_kvish6_report_dates_report(self):
        self.login_and_get_in_kvish6_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_dates_report_span()
        self.remove_logs_dir = True

    def test_kvish6_report_no_reports_alert(self):
        self.login_and_get_in_kvish6_report()
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_current_time_to_select_and_check(GeneralReportsPage.start_hours_select,
                                                     GeneralReportsPage.start_minutes_select)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.no_report_alert)
        self.remove_logs_dir = True

    def test_kvish6_report_reports_table_paging(self):
        self.login_and_get_in_kvish6_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input, months=-4)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        ul_paging = self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.paging_ul)
        if self.driver.wait.wait_for_element_to_be_present(Kvish6ReportPage.first_li,
                                                           raise_exception=False, timeout=10):
            all_ul_buttons = self.driver.wait.wait_for_elements_to_be_present(GeneralReportsPage.table_buttons,
                                                                              driver=ul_paging)
            for button in all_ul_buttons:
                assert button.click, "button " + button.text + " is not clickable"
        self.remove_logs_dir = True
