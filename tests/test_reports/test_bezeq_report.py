import pytest

from tests.test_reports.base_reports_test_class import *


class TestBezeqReport(BaseReportsTestClass):

    def login_and_get_in_bezeq_report(self):
        self.login_and_get_in_report("7", "לבזק")

    def input_phone_number_and_check(self):
        self.driver.tools.set_text(BezeqReportPage.phone_number_input, User.phone)
        assert User.phone in self.driver.tools.wait_for_element_and_get_attribute(
            BezeqReportPage.phone_number_input, "value")

    def input_transaction_number_and_check(self):
        self.driver.tools.set_text(BezeqReportPage.transaction_number_input, User.transaction_number)
        assert User.transaction_number in self.driver.tools.wait_for_element_and_get_attribute(
            BezeqReportPage.transaction_number_input, "value")

    def input_bezeq_number_and_check(self):
        self.driver.tools.set_text(BezeqReportPage.bezeq_number_input, User.bezeq_number)
        assert User.bezeq_number in self.driver.tools.wait_for_element_and_get_attribute(
            BezeqReportPage.bezeq_number_input, "value")

    def test_bezeq_report_get_in_bezeq_report(self):
        self.login_and_get_in_bezeq_report()
        self.remove_logs_dir = True

    def test_bezeq_report_input_phone_number(self):
        self.login_and_get_in_bezeq_report()
        self.input_phone_number_and_check()
        self.remove_logs_dir = True

    def test_bezeq_report_input_transaction_number(self):
        self.login_and_get_in_bezeq_report()
        self.input_transaction_number_and_check()
        self.remove_logs_dir = True

    def test_bezeq_report_input_bezeq_number(self):
        self.login_and_get_in_bezeq_report()
        self.input_bezeq_number_and_check()
        self.remove_logs_dir = True

    def test_bezeq_report_input_start_date(self):
        self.login_and_get_in_bezeq_report()
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.remove_logs_dir = True

    def test_bezeq_report_input_end_date(self):
        self.login_and_get_in_bezeq_report()
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_bezeq_report_start_hour(self):
        self.login_and_get_in_bezeq_report()
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_bezeq_report_start_minute(self):
        self.login_and_get_in_bezeq_report()
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select, minute=2)
        self.remove_logs_dir = True

    def test_bezeq_report_end_hour(self):
        self.login_and_get_in_bezeq_report()
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_bezeq_report_end_minute(self):
        self.login_and_get_in_bezeq_report()
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select, minute=2)
        self.remove_logs_dir = True

    def test_bezeq_report_bezeq_show_reports_btn(self):
        self.login_and_get_in_bezeq_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - reports are currently not shown")
    def test_bezeq_report_sales_summery_span(self):
        self.login_and_get_in_bezeq_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_total_sales_span("דוחתשלוםלבזק")
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - reports are currently not shown")
    def test_bezeq_report_dates_report(self):
        self.login_and_get_in_bezeq_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_dates_report_span()
        self.remove_logs_dir = True

    def test_bezeq_report_no_reports_alert(self):
        self.login_and_get_in_bezeq_report()
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_current_time_to_select_and_check(GeneralReportsPage.start_hours_select,
                                                     GeneralReportsPage.start_minutes_select)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.no_report_alert)
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - reports are currently not shown")
    def test_bezeq_report_reports_table_paging(self):
        self.login_and_get_in_bezeq_report()
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input, months=-4)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        ul_paging = self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.paging_ul)
        if self.driver.wait.wait_for_element_to_be_present(BezeqReportPage.first_li,
                                                           raise_exception=False, timeout=10):
            all_ul_buttons = self.driver.wait.wait_for_elements_to_be_present(GeneralReportsPage.table_buttons,
                                                                              driver=ul_paging)
            for button in all_ul_buttons:
                assert button.click, "button " + button.text + " is not clickable"
        self.remove_logs_dir = True
