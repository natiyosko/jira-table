import pytest

from tests.test_reports.base_reports_test_class import *


class TestRavKavReport(BaseReportsTestClass):

    def test_get_into_rav_kav_report(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.remove_logs_dir = True

    def test_check_inputs_date_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_change_start_end_date_input_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_hour_minute_select_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_deal_number_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.insert_number_to_input_and_verify(99)

    def test_show_reports_btn_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_alert_no_report_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.insert_current_time_to_select_and_check(GeneralReportsPage.start_hours_select,
                                                     GeneralReportsPage.start_minutes_select)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.no_report_alert)
        self.remove_logs_dir = True

    def test_show_reports_btn_details_rav_kav(self):
        self.login_and_get_in_report("25", 'דוח רב קו')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.insert_number_to_input_and_verify(99)
        self.click_on_show_reports_btn_and_check()
        self.check_dates_report_span()
        self.check_total_sales_span("דוחרבקו")
        self.remove_logs_dir = True
