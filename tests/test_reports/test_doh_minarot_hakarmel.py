from tests.test_reports.base_reports_test_class import *
import pytest


class TestMinarotHakarmelReport(BaseReportsTestClass):

    def login_and_get_in_minarot_hakarmel_report(self):
        self.login_and_get_in_report("6", 'מנהרות הכרמל')

    def test_login_and_get_in_minarot_hakarmel_report(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.remove_logs_dir = True

    def test_minutes_hours_select_clickability(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_show_reports_btn(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.fill_all_dates_fields_on_report_pages()
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - total_sales_from_table are currently not shown")
    def test_sales_summery_span(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.fill_all_dates_fields_on_report_pages()
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_total_sales_span('דוחמנהרותהכרמל')
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - dates are currently not shown")
    def test_check_date_of_report(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.fill_all_dates_fields_on_report_pages()
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_dates_report_span()
        self.remove_logs_dir = True

    #@pytest.mark.skip(reason="there is a bug - the rows are not displaying by the chosen selected")
    def test_select_btn_rows_per_page(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.fill_all_dates_fields_on_report_pages()
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_select_btn_rows_per_page()
        self.remove_logs_dir = True

    def test_paginator(self):
        self.login_and_get_in_minarot_hakarmel_report()
        self.fill_all_dates_fields_on_report_pages()
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.check_paginator_buttons_of_doh('דוחמנהרותהכרמל')

    # TODO :Need to verify the excel file that be downloaded
