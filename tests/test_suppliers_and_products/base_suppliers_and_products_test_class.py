from tests.base_project_test_class import *


class BaseSuppliersAndProductsTestClass(BaseProjectTestClass):

    def login_and_get_in_suppliers_and_products(self):
        self.login()
        self.driver.tools.wait_and_click(MainSuppliersAndProductPage.suppliers_and_products_link)
        self.driver.wait.wait_for_elements_to_be_present(MainSuppliersAndProductPage.suppliers_and_products_head_line)

    def click_on_a_tab(self, name_of_tab):
        self.driver.tools.wait_and_click((MainSuppliersAndProductPage.suppliers_and_products_tabs[0],
                                          MainSuppliersAndProductPage.suppliers_and_products_tabs[1]
                                          .format(name_of_tab)))

    def click_on_input_and_check_option(self, input_selector, choice_name, raise_exception=True):
        self.driver.tools.wait_and_click(input_selector)
        self.driver.wait.wait_for_element_to_be_present(
            (MainSuppliersAndProductPage.choose_options_from_input_by_name[0],
             MainSuppliersAndProductPage.choose_options_from_input_by_name[1].format(choice_name)),
            raise_exception=raise_exception)

    def get_list_of_options(self, select_path):
        self.driver.tools.wait_and_click(select_path)
        all_options = self.driver.tools.get_text_from_element(select_path)
        options_names = [option.strip() for option in all_options.split('\n')]
        options_names = list(dict.fromkeys(options_names))
        return options_names

    def get_new_name_to_profile(self, select_path):
        length = self.get_list_of_options(select_path)
        return "test{}".format(len(length))

    def insert_new_profile(self, select_path, new_profile_btn, input_new_profile):
        name = self.get_new_name_to_profile(select_path)
        self.driver.tools.wait_and_click(new_profile_btn)
        self.insert_to_input_and_verify(input_new_profile, name)
        assert self.driver.tools.wait_for_element_and_get_attribute(
            input_new_profile, "value") == name, "name not in input"
        return name

    def insert_to_input_and_verify(self, selector, text):
        self.driver.wait.wait_for_element_to_be_visible(selector)
        self.driver.tools.set_text(selector, text)
        assert self.driver.tools.wait_for_element_and_get_attribute(selector, "value") == str(
                                                                     text), "the number you send not in input"
