from tests.test_suppliers_and_products.base_suppliers_and_products_test_class import *


class TestSuppliersProfile(BaseSuppliersAndProductsTestClass):

    def click_on_suppliers_profile_tab(self):
        self.login_and_get_in_suppliers_and_products()
        self.click_on_a_tab("suppliersProfile")

    def check_if_supplier_name_is_checked(self):
        return set([x.is_selected() for x in self.driver.wait.wait_for_elements_to_be_present(
            SuppliersProfilePage.all_check_box_in_table)])

    def choose_supplier_profile_in_input(self, profile_name):
        self.click_on_suppliers_profile_tab()
        self.click_on_input_and_choose_option(SuppliersProfilePage.suppliers_profile_input, profile_name)

    def fill_inputs_in_marketers_tab_and_save(self, profile_name='supplier profile'):
        self.driver.tools.wait_and_click(SuppliersProfilePage.marketers_link)
        self.click_on_input_and_choose_option(SuppliersProfilePage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(SuppliersProfilePage.marketers_input, 'ידידיה נעול')
        self.click_on_input_and_choose_option(SuppliersProfilePage.suppliers_profile_input_in_marketers,
                                              profile_name)
        self.click_on_input_and_choose_option(SuppliersProfilePage.profit_profile_input_in_marketers, 'profit profile')
        self.click_on_input_and_choose_option(SuppliersProfilePage.commission_profile_input_in_marketers,
                                              'commission profil')
        self.driver.tools.wait_and_click(SuppliersProfilePage.save_btn_on_marketers)
        self.driver.wait.wait_for_element_to_be_present(MainSuppliersAndProductPage.any_pop_up_alert)

    def marketers_login(self):
        self.driver.tools.set_text(LoginPage.username, 'נעול')
        self.driver.tools.set_text(LoginPage.password, '1234')
        self.driver.tools.wait_and_click(LoginPage.submit_btn)

    def original_position_of_supplier_profile(self):
        self.choose_supplier_profile_in_input('supplier profile')
        self.driver.tools.wait_and_click(SuppliersProfilePage.checkall_btn_of_table)
        self.driver.tools.wait_and_click(SuppliersProfilePage.save_btn)
        self.driver.wait.wait_for_element_to_be_present(MainSuppliersAndProductPage.any_pop_up_alert)
        self.fill_inputs_in_marketers_tab_and_save()

    def check_companies_in_marketers_login_and_restart(self, comment):
        self.marketers_login()
        self.driver.tools.wait_and_click(SuppliersProfilePage.x_btn_on_commercial)
        names_of_companies = self.driver.wait.wait_for_elements_to_be_present(
            SuppliersProfilePage.all_companies_on_main)
        assert 'פרטנר' in [y.text for y in names_of_companies], comment
        self.restart_driver()

    def make_changes_in_suppliers_profile_and_restart(self):
        self.choose_supplier_profile_in_input('supplier profile')
        self.driver.tools.click_on_web_element_by_js(
            self.driver.wait.wait_for_element_to_be_present((SuppliersProfilePage.check_one_checkbox[0],
                                                            SuppliersProfilePage.check_one_checkbox[1].format('4'))))
        self.driver.tools.wait_and_click(SuppliersProfilePage.save_btn)
        self.fill_inputs_in_marketers_tab_and_save()
        self.restart_driver()

    def number_of_companies_in_marketers_login(self):
        self.marketers_login()
        self.driver.tools.wait_and_click(SuppliersProfilePage.x_btn_on_commercial)
        names_of_companies = self.driver.wait.wait_for_elements_to_be_present(
            SuppliersProfilePage.all_companies_on_main)
        return len(names_of_companies)

    def build_new_profile(self):
        self.click_on_suppliers_profile_tab()
        new_profile_name = self.insert_new_profile(SuppliersProfilePage.suppliers_profile_input,
                                                   SuppliersProfilePage.add_new_profile_btn,
                                                   SuppliersProfilePage.new_profile_input)
        self.click_on_input_and_choose_option(SuppliersProfilePage.job_permission_input, 'מנהל')
        self.driver.tools.wait_and_click(SuppliersProfilePage.clear_btn_of_table)
        self.driver.tools.click_on_web_element_by_js(
            self.driver.wait.wait_for_element_to_be_present((SuppliersProfilePage.check_one_checkbox[0],
                                                             SuppliersProfilePage.check_one_checkbox[1].format('0'))))
        self.driver.tools.click_on_web_element_by_js(
            self.driver.wait.wait_for_element_to_be_present((SuppliersProfilePage.check_one_checkbox[0],
                                                             SuppliersProfilePage.check_one_checkbox[1].format('12'))))
        self.driver.tools.wait_and_click(SuppliersProfilePage.save_btn)
        self.driver.wait.wait_for_element_to_be_present(MainSuppliersAndProductPage.any_pop_up_alert)
        self.fill_inputs_in_marketers_tab_and_save(profile_name=new_profile_name)
        self.restart_driver()

    def test_unit_for_suppliers_profile(self):
        self.choose_supplier_profile_in_input('supplier profile')
        self.driver.tools.wait_and_click(SuppliersProfilePage.clear_input_choice, raise_exception=False)
        self.click_on_input_and_choose_option(SuppliersProfilePage.job_permission_input, 'מנהל')
        assert self.check_if_supplier_name_is_checked() == {True}, 'not all checkbox are checked by default'
        self.driver.tools.wait_and_click(SuppliersProfilePage.clear_btn_of_table)
        assert self.check_if_supplier_name_is_checked() == {False}, 'there is still sum checkbox that are checked'
        self.driver.tools.wait_and_click(SuppliersProfilePage.checkall_btn_of_table)
        assert self.check_if_supplier_name_is_checked() == {True}, 'not all checkbox are checked'
        self.driver.tools.wait_and_click(SuppliersProfilePage.save_btn)
        self.driver.wait.wait_for_element_to_be_present(MainSuppliersAndProductPage.any_pop_up_alert)
        self.remove_logs_dir = True

    def test_integration_for_suppliers_profile(self):
        self.original_position_of_supplier_profile()
        self.restart_driver()
        self.check_companies_in_marketers_login_and_restart('this company dose not exists')
        self.make_changes_in_suppliers_profile_and_restart()
        self.check_companies_in_marketers_login_and_restart('this company already exists')
        self.original_position_of_supplier_profile()
        self.remove_logs_dir = True

    def test_unit_for_new_profile(self):
        self.click_on_suppliers_profile_tab()
        self.driver.tools.wait_and_click(SuppliersProfilePage.add_new_profile_btn)
        assert self.check_if_supplier_name_is_checked() == {False}, 'there is still sum checkbox that are checked'
        self.driver.tools.wait_and_click(SuppliersProfilePage.cancel_btn)
        self.insert_new_profile(SuppliersProfilePage.suppliers_profile_input, SuppliersProfilePage.add_new_profile_btn,
                                SuppliersProfilePage.new_profile_input)
        self.driver.tools.wait_and_click(SuppliersProfilePage.cancel_btn)
        self.driver.wait.wait_for_element_to_be_invisible(SuppliersProfilePage.job_permission_input)
        self.remove_logs_dir = True

    def test_integration_for_new_profile(self):
        first_count_of_companies = self.number_of_companies_in_marketers_login()
        self.restart_driver()
        self.build_new_profile()
        second_count_of_companies = self.number_of_companies_in_marketers_login()
        assert first_count_of_companies > second_count_of_companies, 'There are too many companies'
        self.restart_driver()
        self.original_position_of_supplier_profile()
        self.remove_logs_dir = True
