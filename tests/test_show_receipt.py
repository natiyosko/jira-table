from tests.base_project_test_class import *


class TestShowReceipt(BaseProjectTestClass):

    def check_for_receipts(self):
        self.driver.tools.wait_and_click(ShowReceipt.show_receipt_btn)
        if not self.driver.tools.get_text_from_element(ShowReceipt.no_receipt_element) == 'לא נמצאו חשבוניות':
            self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
                ShowReceipt.checkbox_of_receipt))
            self.driver.tools.wait_and_click(ShowReceipt.send_receipt_to_mail_btn)
            self.driver.tools.set_text(ShowReceipt.email_input, 'yedidya@ravtech.co.il')
            self.driver.tools.wait_and_click(ShowReceipt.send_email_btn)
            assert self.driver.wait.wait_for_element_to_be_present(
                MainSuppliersAndProductPage.any_pop_up_alert) is not False, 'Email did not sent!!!'

    def test_show_receipt_tab(self):
        self.login_and_click_on_side_menu_option('הצגת חשבוניות')
        self.driver.tools.wait_and_click(ShowReceipt.show_receipt_btn)
        SelectElement(self.driver.driver, self.driver.wait, ShowReceipt.choose_year_input).choose_option_by_text("2019")
        SelectElement(self.driver.driver, self.driver.wait, ShowReceipt.choose_month_input).choose_option_by_text("הכל")
        self.check_for_receipts()
        self.driver.tools.wait_and_click(ShowReceipt.download_adobe_reader_btn)
        self.driver.tools.switch_window(window_position=1)
        self.driver.wait.wait_for_element_to_be_present(ShowReceipt.element_in_adobe_reader_page)
        self.remove_logs_dir = True


