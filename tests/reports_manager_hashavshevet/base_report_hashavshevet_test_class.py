from tests.test_reports.base_reports_test_class import *


class BaseReportsHashavshevetTestClass(BaseReportsTestClass):

    def click_on_checkbox_input_and_check_if_checked(self, checkbox_name, attribute, is_checked=None):
        self.driver.tools.wait_and_click(GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_click)
        self.check_if_checkbox_input_is_checked(checkbox_name=checkbox_name, attribute=attribute, is_checked=is_checked)

    def check_if_checkbox_input_is_checked(self, checkbox_name, attribute, is_checked=None):
        assert is_checked == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_check, attribute,
            timeout=50), f'{checkbox_name} is not {is_checked}'

    def return_if_checkbox_checked_or_unchecked(self, checkbox_selector):
        check = self.driver.tools.wait_for_element_and_get_attribute(checkbox_selector, 'checked')
        return check

    def click_checkbox_hashavshevet(self, input_selector, checkbox_label_locator):
        self.driver.wait.wait_for_element_to_be_present(input_selector)
        self.return_if_checkbox_checked_or_unchecked(input_selector)
        self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
            checkbox_label_locator))
