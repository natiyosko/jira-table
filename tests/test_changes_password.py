from tests.base_project_test_class import *


class TestChangePassword(BaseProjectTestClass):

    def get_in_change_password_area(self):
        self.driver.tools.wait_and_click(HomePage.user_btn)
        self.driver.tools.wait_and_click(HomePage.user_area)
        self.driver.tools.wait_and_click(PersonalAreaPage.change_password_button)

    def change_password(self, current_password, new_password):
        self.driver.tools.set_text(PersonalAreaPage.input_current_password, current_password)
        self.driver.tools.set_text(PersonalAreaPage.input_new_password, new_password)
        self.driver.tools.set_text(PersonalAreaPage.input_new_password_again, new_password)
        self.driver.tools.wait_and_click(PersonalAreaPage.update_password_button)
        self.driver.wait.wait_for_element_to_be_present(GeneralLink.alert_successful)


    def test_change_password_login_get_in_and_exit_from_change_password(self):
        self.login_with_different_username_or_password(User.naull_username, User.naull_password)
        self.driver.tools.wait_and_click(HomePage.exit_adding)
        self.get_in_change_password_area()
        self.driver.tools.wait_and_click(PersonalAreaPage.exit_changes)
        self.driver.wait.wait_for_element_to_be_present(PersonalAreaPage.change_password_button)
        self.remove_logs_dir = True

    def test_change_password_login_and_change_password(self):
        self.login_with_different_username_or_password(User.naull_username, User.naull_password)
        self.driver.tools.wait_and_click(HomePage.exit_adding)
        self.get_in_change_password_area()
        self.change_password('1234', '4321')
        self.restart_driver()
        self.login_with_different_username_or_password(User.naull_username, User.naull_password)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.login_error_msg)
        self.driver.tools.clear_and_set_text(LoginPage.password, User.naull_new_password)
        self.driver.tools.wait_and_click(LoginPage.submit_btn)
        self.driver.tools.wait_and_click(HomePage.exit_adding)
        self.get_in_change_password_area()
        self.change_password('4321', '1234')
        self.remove_logs_dir = True