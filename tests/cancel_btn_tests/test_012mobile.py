import pytest

from tests.base_project_test_class import *


class Test012mobileCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_012mobile(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.zero_one_two_mobile_icon)

    def test_cancel_btn_012mobile_callingcard_25(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(25, "201/1506", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_012mobile_callingcard_45(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(45, "201/1500", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_012mobile_callingcard_50(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(50, "201/1501", cancel_payment=True)
        self.remove_logs_dir = True
