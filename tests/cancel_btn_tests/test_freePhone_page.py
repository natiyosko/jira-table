from tests.base_project_test_class import *

class TestFreePhoneCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_freePhone(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.freePhone_icon)


    def test_cancel_btn_freePhone_talkman_25(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(25, "202/25", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_freePhone_talkman_50(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(50, "202/50", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_freePhone_talkman_100(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(100, "202/100", cancel_payment=True)
        self.remove_logs_dir = True
