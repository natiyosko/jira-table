from tests.base_project_test_class import *

class TestPartnerTalkmansCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_partner(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.partner_icon)

    def test_cancel_btn_partner_talkman_29_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(29, "127/virtual/1024", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_partner_talkman_50_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(50, "127/virtual/1025", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_partner_talkman_59_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(59, "127/virtual/1026", cancel_payment=True)
        self.remove_logs_dir = True

