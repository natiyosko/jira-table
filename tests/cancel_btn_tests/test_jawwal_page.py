import pytest

from tests.base_project_test_class import *

class TestJawwalTalkmanVirtualCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_jawwal(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.jawwal_icon)


    def test_cancel_btn_jawwal_talkman_12(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(12, "210/10", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_jawwal_talkman_22(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(22, "210/20", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_jawwal_talkman_33(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(33, "210/30", cancel_payment=True)
        self.remove_logs_dir = True
