from tests.base_project_test_class import *

class TestBenleumiHomeCardCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_benleumiHomeCard(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.benleumiHomeCard_icon)

    def test_benleumiHomeCard_talkman_20(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(20, "211/2002", cancel_payment=True)
        self.remove_logs_dir = True

    def test_benleumiHomeCard_talkman_30(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(30, "211/2003", cancel_payment=True)
        self.remove_logs_dir = True

    def test_benleumiHomeCard_talkman_74(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(74, "211/2026", cancel_payment=True)
        self.remove_logs_dir = True
