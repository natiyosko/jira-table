import os


class User:
    username = os.environ.get("PELETOK_USERNAME", "peletok0")
    password = os.environ.get("PELETOK_PASSWORD", "pass0")
    false_username = 'ABC'
    false_password = 'null'
    phone = '9999999999'
    id_user = '000000000'
    last_six_digits_of_invoice = '123456'
    carmel_tunnels_ID_for = '000000000'
    contract_number = '5843212'
    phone_meshavkim = '1111111111'
    add_phone_meshavkim = '1111112'
    num_business_authorized_dealer = '123456'
    transaction_number = "9999999999"
    bezeq_number = "999999999"
    naull_username = "נעול"
    naull_password = '1234'
    naull_new_password = '4321'
