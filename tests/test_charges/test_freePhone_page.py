from tests.base_project_test_class import *


class TestFreePhoneCards(BaseProjectTestClass):

    def login_and_get_to_freePhone(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.freePhone_icon)

    def test_freePhone_click_on_back_button(self):
        self.login_and_get_to_freePhone()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_FreePhone_page(self):
        self.login_and_get_to_freePhone()
        self.remove_logs_dir = True

    def test_freePhone_talkman_25(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(25, "202/25")
        self.remove_logs_dir = True

    def test_freePhone_talkman_50(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(50, "202/50")
        self.remove_logs_dir = True

    def test_freePhone_talkman_100(self):
        self.login_and_get_to_freePhone()
        self.pay_for_calling_card_vc(100, "202/100")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_FreePhone(self):
        self.login_and_get_to_freePhone()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_FreePhone(self):
        self.login_and_get_to_freePhone()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_FreePhone(self):
        self.login_and_get_to_freePhone()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
