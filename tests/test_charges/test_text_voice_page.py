from tests.base_project_test_class import *


class TestTextVoiceTalkmanVirtualCards(BaseProjectTestClass):

    def login_and_get_to_text_voice(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.text_voice_icon)

    def test_text_voice_click_on_back_button(self):
        self.login_and_get_to_text_voice()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_text_voice_talkman_29(self):
        self.login_and_get_to_text_voice()
        self.pay_for_calling_card_vc(164, "196/3")
        self.remove_logs_dir = True

    def test_text_voice_talkman_164(self):
        self.login_and_get_to_text_voice()
        self.pay_for_calling_card_vc(29, "196/1")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_text_voice(self):
        self.login_and_get_to_text_voice()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_text_voice(self):
        self.login_and_get_to_text_voice()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_text_voice(self):
        self.login_and_get_to_text_voice()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
