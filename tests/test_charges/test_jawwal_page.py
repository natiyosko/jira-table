from tests.base_project_test_class import *


class TestJawwalTalkmanVirtualCards(BaseProjectTestClass):

    def login_and_get_to_jawwal(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.jawwal_icon)

    def test_jawwal_click_on_back_button(self):
        self.login_and_get_to_jawwal()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_jawwal_page(self):
        self.login_and_get_to_jawwal()
        self.remove_logs_dir = True

    def test_jawwal_talkman_12(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(12, "210/10")
        self.remove_logs_dir = True

    def test_jawwal_talkman_22(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(22, "210/20")
        self.remove_logs_dir = True

    def test_jawwal_talkman_33(self):
        self.login_and_get_to_jawwal()
        self.pay_for_calling_card_vc(33, "210/30")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_jawwal(self):
        self.login_and_get_to_jawwal()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_jawwal(self):
        self.login_and_get_to_jawwal()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_jawwal(self):
        self.login_and_get_to_jawwal()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
