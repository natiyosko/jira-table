from tests.base_project_test_class import *


class TestTehinatChashmalPage(BaseProjectTestClass):

    def login_and_get_to_tehinat_chashmal(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.tehinatChashmal_icon)

    def test_tehinat_chashmal_click_on_back_button(self):
        self.login_and_get_to_tehinat_chashmal()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def input_contract_number(self):
        self.driver.tools.set_text(TehinatChashmalPage.tehinatChashmal_contract_number, User.contract_number)

    def input_amount_to_charge(self):
        self.driver.tools.set_text(TehinatChashmalPage.tehinatChashmal_amount_money_to_charge, User.phone[0])

    def verify_total_payment(self):
        commission_full_text = self.driver.tools.wait_for_element_and_get_attribute(TehinatChashmalPage.commission_cost,
                                                                                    attribute='textContent')
        commission_num_only = commission_full_text.split(' ')
        for d in commission_num_only:
            if d.replace('.', '').isdigit():
                assert int(d) + int(self.driver.tools.wait_for_element_and_get_attribute(
                    TehinatChashmalPage.tehinatChashmal_amount_money_to_charge,
                    attribute='value') in self.driver.tools.wait_for_element_and_get_attribute(
                    TehinatChashmalPage.tehinatChashmal_total_payment,
                    attribute='value')), "failed"

    def input_phone_number(self):
        self.driver.tools.set_text(TehinatChashmalPage.tehinatChashmal_phone_pre_number, User.phone[:3])
        self.driver.tools.set_text(TehinatChashmalPage.tehinatChashmal_phone_senven_number, User.phone[2:])

    def charge_and_verify(self):
        self.driver.tools.wait_and_click(TehinatChashmalPage.tehinatChashmal_charge_button)
        self.driver.wait.wait_for_element_to_be_present(TehinatChashmalPage.tehinatChashmal_success_msg)
        self.driver.tools.wait_and_click(TehinatChashmalPage.tehinatChashmal_cancelButton)

    #@pytest.mark.skip(reason="this test fails for now")
    def test_check_tehinat_chashmal_page_and_verify(self):
        self.login_and_get_to_tehinat_chashmal()
        self.input_contract_number()
        self.input_amount_to_charge()
        self.verify_total_payment()
        self.input_phone_number()
        self.charge_and_verify()
        self.remove_logs_dir = True
