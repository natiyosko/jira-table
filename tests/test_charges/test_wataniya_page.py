from tests.base_project_test_class import *


class TestWataniyaTalkmanCards(BaseProjectTestClass):

    def login_and_get_to_wataniya(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.wataniya_icon)

    def test_wataniya_click_on_back_button(self):
        self.login_and_get_to_wataniya()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_wataniya_page(self):
        self.login_and_get_to_wataniya()
        self.remove_logs_dir = True

    def test_wataniya_talkman_10(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(10, "209/1020")
        self.remove_logs_dir = True

    def test_wataniya_talkman_17(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(17, "209/1021")
        self.remove_logs_dir = True

    def test_wataniya_talkman_25(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(25, "209/1022")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_wataniya(self):
        self.login_and_get_to_wataniya()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_wataniya(self):
        self.login_and_get_to_wataniya()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_wataniya(self):
        self.login_and_get_to_wataniya()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
