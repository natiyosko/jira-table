from tests.base_project_test_class import *


class TestPartnerTalkmansCards(BaseProjectTestClass):

    def login_and_get_to_partner(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.partner_icon)

    def test_partner_click_on_back_button(self):
        self.login_and_get_to_partner()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_partner_page(self):
        self.login_and_get_to_partner()
        self.remove_logs_dir = True

    def test_partner_talkman_29_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(29, "127/virtual/1024")
        self.remove_logs_dir = True

    def test_partner_talkman_50_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(50, "127/virtual/1025")
        self.remove_logs_dir = True

    def test_partner_talkman_59_vc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_vc(59, "127/virtual/1026")
        self.remove_logs_dir = True

    def test_partner_talkman_17_mc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_mc(17, "127/manual/101")
        self.remove_logs_dir = True

    def test_partner_talkman_60_mc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_mc(60, "127/manual/994")
        self.remove_logs_dir = True

    def test_partner_talkman_29_mc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_mc(29, "127/manual/995")
        self.remove_logs_dir = True

    def test_partner_talkman_50_mc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_mc(50, "127/manual/996")
        self.remove_logs_dir = True

    def test_partner_talkman_40_mc(self):
        self.login_and_get_to_partner()
        self.pay_for_calling_card_mc(40, "127/manual/102")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_partner(self):
        self.login_and_get_to_partner()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_partner(self):
        self.login_and_get_to_partner()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_partner(self):
        self.login_and_get_to_partner()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
