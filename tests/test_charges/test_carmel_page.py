from tests.base_project_test_class import *


class TestCarmelTunnelsPage(BaseProjectTestClass):

    def login_and_get_to_carmel_tunnels(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.carmel_tunnels_icon)

    def test_carmel_tunnels_click_on_back_button(self):
        self.login_and_get_to_carmel_tunnels()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_carmel_tunnels_page(self):
        self.login_and_get_to_carmel_tunnels()
        self.driver.tools.set_text(VerificationPage.carmel_tunnels_input_id, User.carmel_tunnels_ID_for)
        self.driver.tools.set_text(VerificationPage.carmel_tunnels_input_last_six_digits_on_invoice_account,
                                   User.phone[:6])
        self.driver.tools.set_text(VerificationPage.carmel_tunnels_input_invoice_account,
                                   User.phone[:2])
        self.input_phone_number()
        self.verify_total_payment()
        self.driver.tools.wait_and_click(VerificationPage.carmel_tunnels_button_charge)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.carmel_tunnels_success_msg)
        self.remove_logs_dir = True

    def input_phone_number(self):
        self.driver.tools.set_text(VerificationPage.carmel_tunnels_input_pre_number, User.phone[:3])
        self.driver.tools.set_text(VerificationPage.carmel_tunnels_input_number, User.phone[3:])

    def verify_total_payment(self):
        full_content_of_the_check_amla = self.driver.tools.wait_for_element_and_get_attribute(
            VerificationPage.carmel_tunnels_commission_a_payment, attribute="textContent")
        only_the_number_of_the_amla = full_content_of_the_check_amla.split(' ')
        for the_num_of_amla in only_the_number_of_the_amla:
            if the_num_of_amla.replace('.', '').isdigit():
                payment = self.driver.tools.wait_for_element_and_get_attribute(
                    VerificationPage.carmel_tunnels_input_invoice_account, attribute="value")
                total_payment = self.driver.tools.wait_for_element_and_get_attribute(
                    VerificationPage.carmel_tunnels_total_payment, attribute="value")
                assert int(the_num_of_amla) + int(payment) == int(total_payment)
