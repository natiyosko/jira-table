from tests.base_project_test_class import *


class TestGlobalSim(BaseProjectTestClass):

    def login_and_get_to_globalSim(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.globalSim_icon)

    def test_globalSim_click_on_back_button(self):
        self.login_and_get_to_globalSim()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_globalSim_page(self):
        self.login_and_get_to_globalSim()
        self.remove_logs_dir = True

    def test_globalSim_talkman_69(self):
        self.login_and_get_to_globalSim()
        self.pay_for_calling_card_vc(69, "203/virtual/2032")
        self.remove_logs_dir = True

    def test_globalSim_talkman_3_mc(self):
        self.login_and_get_to_globalSim()
        self.pay_for_calling_card_mc(3, "203/manual/2034")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_globalSim(self):
        self.login_and_get_to_globalSim()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_globalSim(self):
        self.login_and_get_to_globalSim()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_globalSim(self):
        self.login_and_get_to_globalSim()
        self.wait_and_click_if_virtual()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True