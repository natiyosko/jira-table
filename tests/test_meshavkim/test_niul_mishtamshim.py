from tests.test_meshavkim.base_meshavkim_test_class import *
from datetime import datetime


class TestNiulMishtamshim(BaseMeshavkimTestClass):

    def login_and_get_to_meshavkim(self):
        self.login_and_get_in_meshavkim()

    def enter_to_niul_mishtamshim(self):
        self.login_and_get_to_meshavkim()
        self.click_on_side_menu_options('ניהול משתמשים')

    def check_the_security_ip(self):
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_check_security_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_security_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_on_secure_ip)
        self.driver.tools.set_text(GeneralMeshavkimPage.niul_mishtamshim_number_address_ip, 2)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_button_to_save_the_number_address)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def click_cancel_on_the_delete_security_ip_alert(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_delete_the_security_ip)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_cancel_in_the_alert_cancel)

    def click_approval_on_the_delete_security_ip_alert(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_delete_the_security_ip)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_approval_in_the_alert_cancel)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def check_the_security_device(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_on_secure_device)
        self.driver.tools.set_text(GeneralMeshavkimPage.niul_mishtamshim_number_of_tokens, 2)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_button_to_save_the_number_tokens)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def click_cancel_on_the_delete_security_device_alert(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_delete_the_security_device)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_cancel_in_the_alert_cancel)

    def click_approval_on_the_delete_security_device_alert(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_delete_the_security_device)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_approval_in_the_alert_cancel)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def click_to_get_outside_from_the_security_and_detail_button(self):
        self.driver.tools.wait_and_click(
            GeneralMeshavkimPage.niul_mishtamshim_cancel_from_the_security_and_details_button)

    def click_on_freeze_button(self):
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_check_freeze_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_freeze_button)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def click_on_freeze_cancel_button(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_freeze_button_cancel)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def input_the_name_and_last_name_to_function_put_details(self):
        editing_the_name = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_input_name, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_input_name, editing_the_name)
        editing_the_last_name = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_last_name, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_last_name,
                                             editing_the_last_name)

    def input_the_id_and_number_to_function_put_details(self):
        editing_the_id = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_id, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_id, editing_the_id)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_approval_alert)
        editing_the_number = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_number, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_number, editing_the_number)

    def input_the_mail_and_id_validity_to_function_put_details(self):
        editing_the_mail = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_mail, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_mail, editing_the_mail)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_approval_alert)
        editing_the_validity_id = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_validity_id, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_validity_id,
                                             editing_the_validity_id)

    def input_the_address_and_name_to_function_put_details(self):
        editing_the_address = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_address, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_address, editing_the_address)
        editing_the_name = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.niul_mishtamshim_editing_shem_mishtamesh, attribute='innerText')
        self.driver.tools.clear_and_set_text(GeneralMeshavkimPage.niul_mishtamshim_editing_shem_mishtamesh,
                                             editing_the_name)

    def click_on_permission_and_meshavek_and_password_to_function_put_details(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_editing_permissions)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_editing_click_on_meshavek)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_editing_change_password_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_cancel_in_the_change_password)
        self.driver.tools.wait_and_click(
            GeneralMeshavkimPage.niul_mishtamshim_editing_history_of_changes_password_button)

    def click_on_history_save_editing_and_success_msg_to_function_put_details(self):
        self.driver.tools.wait_and_click(
            GeneralMeshavkimPage.niul_mishtamshim_click_on_close_button_to_close_the_history)
        self.driver.tools.wait_and_js_click(GeneralMeshavkimPage.niul_mishtamshim_save_the_editing)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)

    def function_the_put_details_on_editing(self):
        self.input_the_name_and_last_name_to_function_put_details()
        self.input_the_id_and_number_to_function_put_details()
        self.input_the_mail_and_id_validity_to_function_put_details()
        self.input_the_address_and_name_to_function_put_details()
        self.click_on_permission_and_meshavek_and_password_to_function_put_details()
        self.click_on_history_save_editing_and_success_msg_to_function_put_details()

    def click_button_on_add_user_and_cancel(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_add_user_and_cancel)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_add_user_and_cancel)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_approval_alert)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_add_user_and_cancel)

    def check_the_button_details_for_the_function_check_the_details_button(self):
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_check_details_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_details_button)
        self.click_to_get_outside_from_the_security_and_detail_button()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_details_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_editing_button)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_save_the_editing)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.niul_mishtamshim_success_msg)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_check_details_button)

    def add_name_id_phone_mail_to_function_thet_add_user(self):
        self.insert_full_user_name()
        self.insert_user_id_with_invalid_id()
        self.insert_user_id_with_valid_id()
        self.insert_user_phone_num()
        self.insert_user_mail_with_invalid_mail()
        self.insert_user_mail_with_valid_mail()
        self.insert_id_valid_date()

    def add_user_name_password_and_permission_to_function_thet_add_user(self):
        self.insert_user_address()
        self.insert_user_name()
        self.insert_user_password()
        self.select_user_permission()

    def get_into_the_user_for_table_in_niul_mishtamshim(self):
        self.driver.tools.set_text(LoginPage.username, 'naty')
        self.driver.tools.set_text(LoginPage.password, '1234')
        self.driver.tools.wait_and_click(LoginPage.submit_btn)
        self.driver.tools.wait_and_click(HomePage.zero_one_two_mobile_icon)

    def make_a_buy_for_check_the_table_in_niul_mishtamshim(self):
        self.wait_and_click_if_virtual()
        self.balance_price = float(50)
        self.get_details("201/1501", 50)
        self.click_on_calling_card_by_price(50)
        self.input_phone_number()
        self.verify_alert_phone_number(VerificationPage.alert_phone_number)
        self.driver.tools.wait_and_click(VerificationPage.alert_confirm_btn)
        self.verify_the_verification_number()
        self.add_user_number()
        self.driver.tools.wait_and_click(VerificationPage.virtual_charge_button)
        self.check_if_the_price_on_success_msg_matches(50)
        self.close_the_success_alert()

    def check_if_the_table_in_niul_mishtamshim_is_right(self, verification, the_details, flag):
        details_in_table = self.driver.tools.wait_for_element_and_get_attribute(
            verification, attribute='innerText')
        if flag:
            assert details_in_table == the_details, 'the details not equal to the table'
        else:
            assert details_in_table > the_details, 'the details not equal to the table'

    def the_func_make_the_assert_for_the_table(self):
        self.check_if_the_table_in_niul_mishtamshim_is_right(
            GeneralMeshavkimPage.niul_mishtamshim_from_db_users_num_of_list, '5', True)
        self.check_if_the_table_in_niul_mishtamshim_is_right(
            GeneralMeshavkimPage.niul_mishtamshim_from_db_users_user_name, 'naty', True)
        self.check_if_the_table_in_niul_mishtamshim_is_right(GeneralMeshavkimPage.niul_mishtamshim_from_db_users_phone,
                                                             '0506852868', True)
        self.check_if_the_table_in_niul_mishtamshim_is_right(GeneralMeshavkimPage.niul_mishtamshim_from_db_users_sales,
                                                             '0', False)
        the_string_of_today = datetime.today().strftime('%Y-%m-%d')
        self.check_if_the_table_in_niul_mishtamshim_is_right(
            GeneralMeshavkimPage.niul_mishtamshim_from_db_users_the_date_of_today,
            the_string_of_today, True)

    def test_niul_mishtamshim_then_check_security_button_on_list(self):
        self.enter_to_niul_mishtamshim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.check_the_security_ip()
        self.click_cancel_on_the_delete_security_ip_alert()
        self.click_approval_on_the_delete_security_ip_alert()
        self.check_the_security_device()
        self.click_cancel_on_the_delete_security_device_alert()
        self.click_approval_on_the_delete_security_device_alert()
        self.click_to_get_outside_from_the_security_and_detail_button()
        self.remove_logs_dir = True

    def test_niul_mishtamshim_thet_check_the_details_button_on_list(self):
        self.enter_to_niul_mishtamshim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.check_the_button_details_for_the_function_check_the_details_button()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_editing_button)
        self.function_the_put_details_on_editing()
        self.remove_logs_dir = True

    def test_niul_mishtamshim_thet_check_the_freeze_button_on_list(self):
        self.enter_to_niul_mishtamshim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.click_on_freeze_button()
        self.click_on_freeze_cancel_button()
        self.remove_logs_dir = True

    def test_niul_mishtamshim_then_add_user(self):
        self.enter_to_niul_mishtamshim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.click_button_on_add_user_and_cancel()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.niul_mishtamshim_click_to_add_after_all_add_details)
        self.remove_logs_dir = True

    def test_that_does_integration_on_add_user(self):
        self.get_into_the_user_for_table_in_niul_mishtamshim()
        self.make_a_buy_for_check_the_table_in_niul_mishtamshim()
        self.restart_driver()
        self.enter_to_niul_mishtamshim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.the_func_make_the_assert_for_the_table()
        self.remove_logs_dir = True
