from tests.test_meshavkim.test_netunay_choze import *


class TestCheckBalanceWithANewBusiness(BaseMeshavkimTestClass):

    def test_check_balance_with_a_new_business(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options('נתוני חוזה')
        self.choose_distributor()
        self.choose_marketers()
        self.input_filter_client_number_and_click_btn_search(987)
        self.check_and_fill_out_the_update_balance("QA")
        self.restart_driver_and_get_in_marketers_login()
        self.try_to_buy_more_than_the_balance()
        self.remove_logs_dir = True

    def test_check_frame_with_a_new_business(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options('נתוני חוזה')
        self.choose_distributor()
        self.choose_marketers()
        self.input_filter_client_number_and_click_btn_search(987)
        self.update_frame_amount_and_reset_balance("QA")
        self.restart_driver_and_get_in_marketers_login()
        self.try_to_buy_more_than_the_balance()
        self.remove_logs_dir = True

    def choose_distributor(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')

    def choose_marketers(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'בדיקת יתרה')

    def check_and_fill_out_the_update_balance(self, write_a_note):
        self.driver.tools.clear_value_and_set_text(CheckBalancePage.input_number_frame, 0)
        the_balance = self.driver.tools.wait_for_element_and_get_attribute(GeneralMeshavkimPage.the_amount_of_balance,
                                                                           attribute='value')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_update_balance)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_total_received, '-' + the_balance.replace('-', ''))
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_receipt_number,
                                   Meshavkim_users.numbers_for_use[1])
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_search_list_authority, 'ידידיה שווארץ')
        self.driver.tools.set_text(GeneralMeshavkimPage.input_text_contract_info_note, write_a_note)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_save_the_update_balance)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_update_balance)
        self.driver.tools.clear_value_and_set_text(GeneralMeshavkimPage.input_contract_info_total_received, 700)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_receipt_number,
                                   Meshavkim_users.numbers_for_use[1])
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_search_list_authority, 'ידידיה שווארץ')
        self.driver.tools.set_text(GeneralMeshavkimPage.input_text_contract_info_note, write_a_note)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_save_the_update_balance)

    def restart_driver_and_get_in_marketers_login(self):
        self.restart_driver()
        self.marketers_login()

    def marketers_login(self):
        self.driver.tools.set_text(LoginPage.username, 'בדיקת יתרה')
        self.driver.tools.set_text(LoginPage.password, '1234')
        self.driver.tools.wait_and_click(LoginPage.submit_btn)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.x_btn_on_commercial)

    def try_to_buy_more_than_the_balance(self):
        self.get_in_electricity_charge()
        corrent_balance = self.take_digits_from_string_and_get_down_the_commas(
            self.driver.tools.wait_for_element_and_get_attribute(BalanceVerification.balance_itra_lenitzul,
                                                                 attribute='innerText'))
        while int(corrent_balance) > int(self.driver.tools.wait_for_element_and_get_attribute(
                CheckBalancePage.total_amount_payment, attribute='innerText')):
            self.driver.tools.wait_and_click(CheckBalancePage.pay_button)
            self.driver.tools.refresh_the_page()
            self.driver.tools.wait_and_click(GeneralMeshavkimPage.x_btn_on_commercial)
            self.get_in_electricity_charge()
            corrent_balance = self.take_digits_from_string_and_get_down_the_commas(
                self.driver.tools.wait_for_element_and_get_attribute(BalanceVerification.balance_itra_lenitzul,
                                                                     attribute='innerText'))
        self.driver.tools.wait_and_click(CheckBalancePage.pay_button)
        self.driver.wait.wait_for_element_to_be_present(CheckBalancePage.alert_credit_limit)

    def get_in_electricity_charge(self):
        self.driver.tools.wait_and_click(CheckBalancePage.cheshbon_chashmal_icon)
        self.driver.tools.set_text(CheckBalancePage.input_contract_number, Meshavkim_users.numbers_for_use[:3])
        self.driver.tools.wait_and_click(CheckBalancePage.electricity_charge_button)
        self.driver.tools.set_text(CheckBalancePage.input_phone_pre_number, Meshavkim_users.numbers_for_use[:3])
        self.driver.tools.set_text(CheckBalancePage.input_phone_number, Meshavkim_users.numbers_for_use[3:])

    def update_frame_amount_and_reset_balance(self, write_a_note):
        self.driver.tools.clear_value_and_set_text(CheckBalancePage.input_number_frame, 700)
        the_balance = self.driver.tools.wait_for_element_and_get_attribute(GeneralMeshavkimPage.the_amount_of_balance,
                                                                           attribute='value')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_update_balance)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_total_received, the_balance.replace('-', ''))
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_receipt_number,
                                   Meshavkim_users.numbers_for_use[1])
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_search_list_authority, 'ידידיה שווארץ')
        self.driver.tools.set_text(GeneralMeshavkimPage.input_text_contract_info_note, write_a_note)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_save_the_update_balance)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_save_btn)

