from tests.test_meshavkim.base_meshavkim_test_class import *


class TestProfitPercentages(BaseMeshavkimTestClass):

    def login_and_get_in_profit_percentages(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options('אחוזי רווח')
        self.driver.wait.wait_for_element_to_be_invisible(GeneralMeshavkimPage.wait_for_spiner_loading)

    def choose_distributor_reseller_supplier_and_get_profit_section(self, distributor, reseller, supplier):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.filter_behirat_mefit, distributor)
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.filter_behirat_meshavek, reseller)
        self.click_on_input_and_choose_option(ProfitPercentagesPage.select_supplier, supplier)
        return self.driver.tools.get_text_from_element(ProfitPercentagesPage.percent_first_row)

    def test_profit_percentages_login_and_get_in_profit_percentages(self):
        self.login_and_get_in_profit_percentages()
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.btn_search)
        self.remove_logs_dir = True

    def test_profit_percentages_login_and_check_profit_section_20_is_changing(self):
        self.login_and_get_in_profit_percentages()
        percentage_profit = self.choose_distributor_reseller_supplier_and_get_profit_section('peletok', 'peletok', 'סלקום  QA')
        vat = self.get_vat()
        self.change_vat(vat+3)
        self.driver.tools.wait_and_click(HomePage.meshavkim_page)
        self.restart_driver()
        self.login_and_get_in_profit_percentages()
        new_percentage_profit = self.choose_distributor_reseller_supplier_and_get_profit_section('peletok', 'peletok', 'סלקום  QA')
        self.change_vat(vat)
        assert percentage_profit != new_percentage_profit, 'Profit percentage section 20 did not change'
        self.remove_logs_dir = True

    def test_login_and_get_to_meshavkim_profit_percentage(self):
        self.login_and_get_in_profit_percentages()
        self.remove_logs_dir = True

    def test_filter_by_mefitz_and_meshavek(self):
        self.login_and_get_in_profit_percentages()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.wait.wait_for_element_to_be_present(ProfitPercentages.btn_save_distribution_fee)
        self.remove_logs_dir = True

    def test_filter_by_phone_number(self):
        self.login_and_get_in_profit_percentages()
        self.input_filter_phone_number_and_click_btn_search()
        self.driver.wait.wait_for_element_to_be_present(ProfitPercentages.btn_save_distribution_fee)
        self.remove_logs_dir = True

    def test_filter_by_authorized_dealer_num(self):
        self.login_and_get_in_profit_percentages()
        self.input_number_business_authorized_dealer_and_click_btn_search()
        self.driver.wait.wait_for_element_to_be_present(ProfitPercentages.btn_save_distribution_fee)
        self.remove_logs_dir = True

    def test_checkbox_use_distribution_fee(self):
        self.login_and_get_in_profit_percentages()
        self.input_number_business_authorized_dealer_and_click_btn_search()
        if not self.driver.tools.click_on_web_element_by_js(
                self.driver.wait.wait_for_element_to_be_present(ProfitPercentages.checkbox_use_distribution_fee)):
            self.driver.tools.wait_and_click(ProfitPercentages.btn_save_distribution_fee)
            self.driver.wait.wait_for_element_to_be_present(ProfitPercentages.alert_success)
            self.remove_logs_dir = True

    def test_radio_btn_vc_mc_and_Payment_of_account(self):
        self.login_and_get_in_profit_percentages()
        self.input_number_business_authorized_dealer_and_click_btn_search()
        for item in reversed(range(3)):
            self.driver.tools.wait_and_click(
                (ProfitPercentages.radio_btn[0], ProfitPercentages.radio_btn[1].format(item)))
            assert self.driver.tools.wait_for_element_and_get_attribute((ProfitPercentages.radio_btn[0],
                                                                         ProfitPercentages.radio_btn[1].format(item))
                                                                        , "checked") == "true", "radio btn is not " \
                                                                                                "select "
        self.remove_logs_dir = True