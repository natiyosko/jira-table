from tests.test_meshavkim.base_meshavkim_test_class import *


class TestMeshavkimPertayEsek(BaseMeshavkimTestClass):

    def login_and_get_to_meshavkim_in_pertay_esek(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options("פרטי עסק")
        self.remove_logs_dir = True

    def test_netunay_choze_in_pertay_esek_and_freeze(self):
        self.pick_chosen_distributor_and_merketers_details()
        self.click_on_search_btn()
        self.check_if_input_client_number_matches()
        self.choose_payment_method()
        self.choose_collection_days()
        self.insert_number_to_earning_points()
        self.insert_number_in_input_frame_and_temporary_frame()
        self.check_and_fill_out_the_update_balance('qa automation test 1')
        self.click_on_distributors_save()
        self.click_to_freeze_distributors()
        self.restart_driver_and_get_in_marketers_login()
        self.check_if_new_user_logged_in()
        self.go_back_to_unfreezes()
        self.remove_logs_dir = True

    def test_netunay_choze_in_pertay_esek_and_unfreezes(self):
        self.pick_chosen_distributor_and_merketers_details()
        self.click_to_unfreeze_distributors()
        self.restart_driver_and_get_in_marketers_login()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.x_btn_on_commercial)
        self.driver.wait.wait_for_element_to_be_clickable(
            GeneralMeshavkimPage.verify_login_with_another_user)
        self.remove_logs_dir = True

    def test_netunay_choze_in_pertay_esek_and_lock(self):
        self.pick_chosen_distributor_and_merketers_details()
        self.click_to_lock_distributors()
        self.restart_driver_and_get_in_marketers_login()
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.user_blocked_msg)
        self.remove_logs_dir = True

    def test_netunay_choze_in_pertay_esek_and_unlock(self):
        self.pick_chosen_distributor_and_merketers_details()
        self.click_on_distributors_to_unlock()
        self.restart_driver_and_get_in_marketers_login()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.x_btn_on_commercial)
        self.driver.wait.wait_for_element_to_be_clickable(
            GeneralMeshavkimPage.verify_login_with_another_user)
        self.remove_logs_dir = True

    def pick_chosen_distributor_and_merketers_details(self):
        self.login_and_get_to_meshavkim_in_pertay_esek()
        self.click_on_side_menu_options('נתוני חוזה')
        self.choose_distributor()
        self.choose_marketers()
        self.input_filter_client_number_and_click_btn_search(1)

    def restart_driver_and_get_in_marketers_login(self):
        self.restart_driver()
        self.marketers_login()

    def choose_distributor(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')

    def choose_marketers(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')

    def click_on_search_btn(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def verify_the_client_number(self):
        assert 1 in self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.input_type_client_number), "the input text doesn't match"

    def choose_payment_method(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.netunay_choze_input_search_payment_method,
                                              'הוראת קבע')

    def choose_collection_days(self):
        name_of_the_and_of_checkbox = ["Alef", "Bet", "Gimel", "Dalet", "Hey", "Vav", "Month", "DelekUser",
                                       "PointsUser"]
        for day in name_of_the_and_of_checkbox:
            attribute_of_checkbox = self.driver.tools.wait_for_element_and_get_attribute((
                GeneralMeshavkimPage.attribute_of_all_checkbox[0],
                GeneralMeshavkimPage.attribute_of_all_checkbox[1].format(day)), "checked")
            if attribute_of_checkbox is None:
                self.driver.tools.wait_and_click((GeneralMeshavkimPage.click_on_all_checkbox[0],
                                                  GeneralMeshavkimPage.click_on_all_checkbox[1].format(day)))

    def insert_number_to_earning_points(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_earning_points, Meshavkim_users.numbers_for_use[1])

    def insert_number_in_input_frame_and_temporary_frame(self):
        self.driver.tools.clear_value_and_set_text(GeneralMeshavkimPage.input_number_frame,
                                                   Meshavkim_users.numbers_for_use[0])
        self.driver.tools.clear_value_and_set_text(GeneralMeshavkimPage.input_number_temporary_frame,
                                                   Meshavkim_users.numbers_for_use[0])

    def check_and_fill_out_the_update_balance(self, write_a_note):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_update_balance)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.cancel_the_update_balance)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_update_balance)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_total_received,
                                   Meshavkim_users.numbers_for_use[0])
        self.driver.tools.set_text(GeneralMeshavkimPage.input_contract_info_receipt_number,
                                   Meshavkim_users.numbers_for_use[0])
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_search_list_authority, 'ידידיה שווארץ')
        self.driver.tools.set_text(GeneralMeshavkimPage.input_text_contract_info_note, write_a_note)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_save_the_update_balance)

    def click_on_distributors_save(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_save_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def click_to_freeze_distributors(self):
        if not self.driver.tools.get_text_from_element(GeneralMeshavkimPage.distributors_freeze_btn) == "הקפאה":
            self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_freeze_btn)
            self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_freeze_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def click_to_unfreeze_distributors(self):
        if not self.driver.tools.get_text_from_element(GeneralMeshavkimPage.distributors_freeze_btn) == "ביטול הקפאה":
            self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_freeze_btn)
            self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_freeze_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def marketers_login(self):
        self.driver.tools.set_text(LoginPage.username, 'נעול')
        self.driver.tools.set_text(LoginPage.password, '1234')
        self.driver.tools.wait_and_click(LoginPage.submit_btn)

    def click_to_lock_distributors(self):
        if not self.driver.tools.get_text_from_element(GeneralMeshavkimPage.distributors_lock_btn) == "נעל":
            self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_lock_btn)
            self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_lock_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def click_on_distributors_to_unlock(self):
        if not self.driver.tools.get_text_from_element(GeneralMeshavkimPage.distributors_lock_btn) == "ביטול נעילה":
            self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_lock_btn)
            self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_lock_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def check_if_new_user_logged_in(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.x_btn_on_commercial)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.business_freeze_alert)

    def go_back_to_unfreezes(self):
        self.restart_driver()
        self.pick_chosen_distributor_and_merketers_details()
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.distributors_freeze_btn)
