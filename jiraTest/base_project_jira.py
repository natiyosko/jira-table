from time import sleep

from appium_selenium_driver.base_test_class import *
from import_pages import *


class BaseProjectJiraClass(BaseTestClass):
    mail_yedidya = "yedidya@ravtech.co.il"
    code_yedidya = "Aa13243546"

    def login_jira(self):
        self.driver.tools.set_text(jiraPeltokPage.username_jira, jiraPeltokPage.mail_yedidya)
        self.driver.tools.wait_and_click(jiraPeltokPage.submit_btn_jira)
        self.driver.tools.set_text(jiraPeltokPage.password_jira, jiraPeltokPage.code_yedidya)
        self.driver.tools.wait_and_click(jiraPeltokPage.submit_btn_jira)
        sleep(10)

    def enter_to_filters(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_filter_button)
        self.driver.tools.wait_and_click(jiraPeltokPage.the_qa_export_button)
        sleep(10)

    def clean_the_filther(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_click_to_choose_project)
        self.driver.tools.wait_and_click(jiraPeltokPage.the_click_to_cancel_noah)
        self.driver.tools.wait_and_click(jiraPeltokPage.the_click_to_choose_peletok)
        self.driver.tools.wait_and_click(jiraPeltokPage.the_click_to_choose_project)
        self.driver.tools.wait_and_click(jiraPeltokPage.the_click_to_cancel_version_button)
        the_number_of_the_result = self.driver.tools.wait_for_element_and_get_attribute(
            jiraPeltokPage.the_number_of_the_result,
            attribute='textContent')
        sleep(10)
        print("\nthe number of the result", the_number_of_the_result)

    def function_to_find_the_mission_done(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("Done")))
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        the_number_of_done = self.driver.tools.wait_for_element_and_get_attribute(
            jiraPeltokPage.the_number_of_the_result,
            attribute='textContent')
        sleep(10)
        print("the number of done", the_number_of_done)

    def function_to_find_the_mission_ToDo(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        self.to_find_done_and_todo()
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        the_number_of_todo = self.driver.tools.wait_for_element_and_get_attribute(
            jiraPeltokPage.the_number_of_the_result,
            attribute='textContent')
        sleep(10)
        print("the number of todo", the_number_of_todo)

    def function_to_find_the_mission_Done_and_ToDo(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("Done")))
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        the_number_of_todo_and_done = self.driver.tools.wait_for_element_and_get_attribute(
            jiraPeltokPage.the_number_of_the_result,
            attribute='textContent')
        sleep(10)
        print("the number of todo and done", the_number_of_todo_and_done)

    def function_to_find_the_mission_in_process(self):
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
        self.to_find_done_and_todo()
        self.to_find_all_result_of_process()
        the_number_of_process = self.driver.tools.wait_for_element_and_get_attribute(
            jiraPeltokPage.the_number_of_the_result,
            attribute='textContent')
        sleep(10)
        print("the number of in process", the_number_of_process)

    def login_to_docs(self):
        self.change_url_to_docs()
        self.driver.tools.set_text(DocsPage.input_mail_to_login_docs, jiraPeltokPage.mail_docs)
        self.driver.tools.wait_and_click(DocsPage.submit_mail)
        self.driver.tools.set_text(DocsPage.input_pass_to_login_docs, jiraPeltokPage.code_docs)
        self.driver.tools.wait_and_click(DocsPage.submit_pass)
        sleep(10)

    def to_find_done_and_todo(self):
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("Done")))
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("To Do")))

    def to_find_all_result_of_process(self):
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("In Code Review 90%")))
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("In Progress 0%")))
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("In Progress 30%")))
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("In Progress 60%")))
        self.driver.tools.wait_and_click(
            (jiraPeltokPage.the_button_in_the_choose_status[0],
             jiraPeltokPage.the_button_in_the_choose_status[1].format("In Test 70%")))
        self.driver.tools.wait_and_click(jiraPeltokPage.the_status_button)
