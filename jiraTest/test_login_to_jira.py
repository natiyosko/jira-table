from jiraTest.base_project_jira import *


class TestLoginJira(BaseProjectJiraClass):

    def test_login_success_jira(self):
        self.login_jira()
        self.enter_to_filters()
        self.clean_the_filther()
        self.function_to_find_the_mission_done()
        self.function_to_find_the_mission_ToDo()
        self.function_to_find_the_mission_Done_and_ToDo()
        self.function_to_find_the_mission_in_process()
        self.login_to_docs()
        self.remove_logs_dir = True

