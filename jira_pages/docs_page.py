from selenium.webdriver.common.by import By


class DocsPage:
    input_mail_to_login_docs = (By.ID, 'identifierId')
    submit_mail = (By.CSS_SELECTOR, '#identifierNext > div > button > div.VfPpkd-RLmnJb')
    input_pass_to_login_docs = (By.XPATH, '//*[@id="password"] //input[@class="whsOnd zHQkBf"]')
    submit_pass = (By.CSS_SELECTOR, '#passwordNext > div > button > div.VfPpkd-RLmnJb')
    sach_kol_mispar_hbagim_shenimtzaim_Done_ = (By.XPATH, '//*[@id="waffle-rich-text-editor"]/span[text()="325"]')
