from selenium.webdriver.common.by import By


class jiraPeltokPage:
    username_jira = (By.ID, "username")
    password_jira = (By.CSS_SELECTOR, "#password")
    submit_btn_jira = (By.ID, "login-submit")
    the_filter_button = (By.CSS_SELECTOR, "div:nth-child(3)>button>span>span.css-t5emrf")
    the_qa_export_button = (By.CSS_SELECTOR, " div.css-1b3ew6t>span:nth-child(1)>a>div>span.css-18l5onf>span")
    the_click_to_choose_project = (By.CSS_SELECTOR, " div.search-criteria>ul>li:nth-child(1)>button")
    the_click_to_cancel_noah = (By.CSS_SELECTOR, "#searcher-pid-suggestions>div>ul.aui-list-section.selected-group")
    the_click_to_choose_peletok = (By.XPATH, '//label[@class="item-label" and contains(text(),"Peletalk (PL)")]')
    the_click_to_cancel_version_button = (By.CSS_SELECTOR, "div.search-criteria-extended>ul>li>a>span")
    the_number_of_the_result = (By.CSS_SELECTOR,
                                "div.aui-group.aui-group-split.issue-table-info-bar>div:nth-child("
                                "1)>span>span.results-count-total.results-count-link")
    the_more_button = (By.CSS_SELECTOR, "li.criteria-actions.new-gin-styling>button")
    the_status_button = (By.CSS_SELECTOR, "div.search-criteria>ul>li:nth-child(3)")
    the_button_in_the_choose_status = (By.XPATH, "//label/span[text()='{}']")

    mail_yedidya = "yedidya@ravtech.co.il"
    code_yedidya = "Aa13243546"
    mail_docs = "yossi.heftler@ravtech.co.il"
    code_docs = "A12345678Zz"
