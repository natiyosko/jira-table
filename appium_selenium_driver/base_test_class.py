from appium_selenium_driver.appium_selenium_driver import Driver
from appium_selenium_driver.desired_capabilities.selenium_desired_capabilities import *
from appium_selenium_driver.report_tools.create_logs_dir import LogsDir

# make main log dir
log_reports = LogsDir(path_to_main_dir_log=os.environ.get('WORKSPACE', None))
log_reports.create_main_logs_dir()


class BaseTestClass:
    driver = None
    # if remove logs and screenshots dir of current test(in case of success test)
    remove_logs_dir = False

    def setup(self):
        pass

    def setup_method(self, method):
        self.remove_logs_dir = False
        test_name = method.__name__
        # make current test logs and screenshots dir
        log_reports.create_test_log_dir(dir_name=test_name)
        self.init_driver()

    def teardown_method(self):
        # stop webdriver for current test
        self.driver.driver.quit()
        # check if need to remove test in case of success test
        if self.remove_logs_dir:
            log_reports.remove_current_test_dir()

    def init_driver(self):
        if os.environ.get("REMOTE_SELENIUM", "False") == "True":
            self.driver = Driver(address="http://selenium:4444/wd/hub", browser_profile="chrome",
                                 desired_capabilities=selenium_remote_docker, remote=True)
        elif os.environ.get("HEADLESS", "False") == "True":
            self.driver = Driver(browser_profile="chrome",
                                 desired_capabilities=selenium_headless, remote=False)
        else:
            self.driver = Driver(browser_profile="chrome", remote=False)

        self.driver.driver.set_window_size(1920, 1080)
        # self.driver.driver.get(self.get_base_url())
        self.driver.driver.get(
            "https://ravtech.atlassian.net/secure/RapidBoard.jspa?rapidView=16&projectKey=PL&sprint=123&quickFilter=90")

    def init_driver_2(self):
        if os.environ.get("REMOTE_SELENIUM", "False") == "True":
            self.driver = Driver(address="http://selenium:4444/wd/hub", browser_profile="chrome",
                                 desired_capabilities=selenium_remote_docker, remote=True)
        elif os.environ.get("HEADLESS", "False") == "True":
            self.driver = Driver(browser_profile="chrome",
                                 desired_capabilities=selenium_headless, remote=False)
        else:
            self.driver = Driver(browser_profile="chrome", remote=False)

        self.driver.driver.set_window_size(1920, 1080)
        self.driver.driver.get(
            "https://docs.google.com/spreadsheets/d/1oaPd3ieYReEfpTHIHCIwatYz4XqOBI49W5RNso5RHBo/edit#gid=0")

    def restart_driver(self):
        self.driver.driver.quit()
        self.init_driver()

    def change_url_to_docs(self):
        self.driver.driver.quit()
        self.init_driver_2()
