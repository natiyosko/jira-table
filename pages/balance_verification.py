from selenium.webdriver.common.by import By


class BalanceVerification:
    genaral_locator = "div.p-1.index.col-sm-20.d-none.d-md-block.col>div>div.p-0.mt-auto.container>div:nth-child({})>" \
                      "div.col-9.py-1.px-2.balanceBoxValue.leftBorderRadius-3.col>"

    balance_misgeret = (By.CSS_SELECTOR, "{}span>span#sum".format(genaral_locator.format(1)))

    balance_misgeret_agurut = (
        By.CSS_SELECTOR, "{}span>span.my-auto.balanceBoxRemainder".format(genaral_locator.format(1)))

    balance_itra = (By.CSS_SELECTOR, "{}span>span#sum".format(genaral_locator.format(2)))

    balance_itra_agurut = (By.CSS_SELECTOR, "{}span>span.my-auto.balanceBoxRemainder".format(genaral_locator.format(2)))

    balance_itra_lenitzul = (By.CSS_SELECTOR, "{}span>span#sum".format(genaral_locator.format(3)))

    balance_itra_lenitzul_agurut = (
        By.CSS_SELECTOR, "{}span>span.my-auto.balanceBoxRemainder".format(genaral_locator.format(3)))

    balance_nekudot_lenitzul = (By.CSS_SELECTOR, "{}span>span#sum".format(genaral_locator.format(4)))

    balance_nekudot_lenitzul_agurut = (
        By.CSS_SELECTOR, "{}span>span.my-auto.balanceBoxRemainder".format(genaral_locator.format(4)))
