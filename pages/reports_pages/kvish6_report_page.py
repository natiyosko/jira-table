from selenium.webdriver.common.by import By


class Kvish6ReportPage:

    title = (By.XPATH, '//h3[contains(text(),"לכביש")]')
    paging_ul = (By.CLASS_NAME, 'pagination')
    first_li = (By.ID, "דוחתשלוםלכביש60")
