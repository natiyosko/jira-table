from selenium.webdriver.common.by import By


class GeneralReportsPage:
    side_menu_options = (By.XPATH, '//div[@class="temp_background main-body pt-5"]'
                                   '//div[@class="px-0" and contains(text(), "{}")]')
    """I add by_href_number """
    report_buttons_by_href_number = (By.XPATH,
                                     '//div[@class="temp_background main-body pt-5"]//a[@href="/main/report/{}"]')
    title = (By.XPATH, '//h3[contains(text(),"{}")]')
    suppliers_input = (By.ID, 'InputTypeSearchListprovider')
    radio_button = (By.XPATH, '//label[@for="InputUtilsRadioservice_type{}"]')
    start_date_input = (By.ID, 'InputTypeDatestartDate')
    end_date_input = (By.ID, 'InputTypeDateendDate')
    start_minutes_select = (By.ID, "InputTypeTimeMinutestartDateTime")
    start_hours_select = (By.ID, "InputTypeTimeHourstartDateTime")
    end_minutes_select = (By.ID, "InputTypeTimeMinuteendDateTime")
    end_hours_select = (By.ID, "InputTypeTimeHourendDateTime")
    deal_number_input = (By.ID, 'InputTypeNumbertransaction')
    show_report_btn = (By.ID, "showReport")
    total_sales_span = (By.ID, "moneySymbol")
    report_dates_span = (By.ID, 'dateOfReport')
    no_report_alert = (By.CLASS_NAME, 'myAlert')
    report_table = (By.XPATH, '//div[@class="collapse show"]//div[@class="collapse show"]')
    paging_ul = (By.CLASS_NAME, 'pagination')
    table_buttons = (By.TAG_NAME, "button")
    end_btn_in_table = (By.ID, '{}End')
    start_btn_in_table = (By.ID, '{}Start')
    previous_btn_in_table = (By.ID, '{}Previous')
    next_btn_in_table = (By.ID, '{}Next')
    digits_btn_in_table = (By.ID, '{}{}')
    total_sales_from_table = (By.CSS_SELECTOR, 'tfoot [id="consumer_price"]')
    yech_report_responsive_table = (By.CLASS_NAME, "table-responsive")
    exportExcelButton = (By.CLASS_NAME, 'exportExcelButton')
    rows_doh_per_pages_select = (By.ID, 'rowsPerPageundefined')
    rows_display_in_reponsive_table = (By.CSS_SELECTOR, "[class='table table-sm table-bordered table-striped'] tbody")
    td_rows_display_in_reponsive_table = (rows_display_in_reponsive_table[0], rows_display_in_reponsive_table[1] + ' td')

