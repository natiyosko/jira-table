from selenium.webdriver.common.by import By


class CommissionPage:

    choose_supplier_input = (By.ID, 'InputTypeSearchListBusinessCommissionsSelectSupplier')
    choose_product_input = (By.ID, 'InputTypeSearchListBusinessCommissionsSelectProduct')
    commission_input = (By.ID, 'InputTypeNumberBusinessCommissionsCommission')
    start_commission_btn = (By.ID, 'BusinessCommissionsSpreadCommissionButton')
    confirm_btn_in_alert = (By.CSS_SELECTOR, 'div button.btn-primary')
    cancel_btn_in_alert = (By.CSS_SELECTOR, 'div.modal-footer > button.btn.btn-secondary')
    final_commission_input = (By.ID, 'InputTypeNumberBusinessCommissionsCommissionForFinalClient')
    start_final_commission_btn = (By.ID, 'BusinessCommissionsSpreadCommissionForFinalClientButton')
    checkall_btn = (By.CSS_SELECTOR, 'div > button.commissionProfileButton.CommissionProfileSmallButton')
    clear_btn = (By.CSS_SELECTOR, 'div > button.CommissionProfileDarkButton.CommissionProfileSmallButton')
    all_checkbox = (By.XPATH, '//*[@type="checkbox"]')
    product_column = (By.XPATH, '//td[@class="111111111" and contains(text(), "תשלום חשבון כביש 6")]')
    commission_column = (By.XPATH, '(//td[@class="111111111" and contains(text(), "תשלום חשבון כביש 6")]'
                                   '/parent ::*//input[@class="numberInput rtl form-control"])[1]')
    final_commission_column = (By.XPATH, '(//td[@class="111111111" and contains(text(), "תשלום חשבון כביש 6")]'
                                         '/parent ::*//input[@class="numberInput rtl form-control"])[2]')
    allowed_to_use_column = (By.XPATH, '//td[@class="111111111" and contains(text(), "תשלום חשבון כביש 6")]'
                                       '/parent ::*//input[@class="custom-control-input form-check-input"]')
    save_btn = (By.ID, 'BusinessCommissionsSaveButton')
    amount_of_commission = (By.CSS_SELECTOR, 'div.mx-1.my-2.commissionNotesRow > div.col')
