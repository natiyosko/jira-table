from selenium.webdriver.common.by import By


class FormKvish6:
    input_id_kvish_6 = (By.ID, 'InputTypeNumberIdInput')
    input_last_six_digits_of_invoice_kvish_6 = (By.ID, 'InputTypeNumberinvoiceInput')
    invoice_amount_kvish_6 = (By.ID, 'InputTypeNumberRecieptAmountInput')
    total_invoice_amount_kvish_6 = (By.ID, 'InputTypeNumberTotalAmountInput')
    phone_number = (By.ID, 'InputTypeNumberInputTypePhoneNumberPhoneNumberInput')
    pre_phone_number = (By.ID, 'InputTypeNumberInputTypePhonePrefixPhoneNumberInput')
    btn_charging_kvish_6 = (By.ID, 'תשלום כביש 6Button')